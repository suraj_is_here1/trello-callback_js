const boards = require("../boards.json");
const boardInfo = require("../callback1.cjs");

try {
    let id = "mcu453ed";

    boardInfo(boards, id, (err, board) => {
        if (err) {
            console.log("Board id not found");
        }
        else {
            console.log(board);
        }
    })
} catch (err) {

    console.log(err);

}