function returnCards(id, cardObject, callback) {

    setTimeout(() => {

        if (typeof (cardObject) === "object" && typeof (id) == "string" && typeof (callback) == "function") {

            let keyExists = Object.keys(cardObject).find(key => key == id)

            if (keyExists) {
                callback(null, cardObject[id])
            }
            else {
                callback(new Error("Data not found", null))
            }
        }
    }, Math.floor(Math.random() * 5) * 1000)
}

module.exports = returnCards;