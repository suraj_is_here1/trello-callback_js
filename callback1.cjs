function boardInfo(boards, id, callback) {
    setTimeout(() => {
        if (Array.isArray(boards) && typeof (id) == "string" && typeof (callback) == "function") {
            let requiredData = boards.find(board => board.id == id);
    
            if (requiredData) {
                callback(null, requiredData)
            }
            else {
                callback(new Error("id not found"), null)
            }
        }
        else {
            console.log("Pass Valid Argument as parameter!");
        }
    },Math.floor(Math.random()*5)*1000)
}

module.exports = boardInfo;