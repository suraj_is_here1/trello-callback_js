function returnLists(id, listObject, callback) {
    setTimeout(() => {
        if (typeof (listObject) === "object" && typeof (id) == "string" && typeof (callback) == "function") {

            let keyExists = Object.keys(listObject).find(key => key == id)
            if (keyExists) {
                callback(null, listObject[id])
            }
            else {
                callback(new Error("Data not found", null))
            }
        }
    },Math.floor(Math.random()*5)*1000)
}

module.exports = returnLists;