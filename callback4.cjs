const boardInfo = require("./callback1.cjs");
const returnLists = require("./callback2.cjs");
const returnCards = require("./callback3.cjs");
const cardObject = require("./cards.json")
const boards = require("./boards.json");
let lists = require("./lists_1.json");



function getAllDatafromMindList() {


    let id = boards.find((data) => data.name === "Thanos").id;

    boardInfo(boards, id, (err, board) => {

        if (err) {

            console.log("Board id not found");
        }
        else {

            console.log(`information from the Thanos boards :-`);
            console.log(board);

            let listId = id;

            returnLists(listId, lists, (err, listData) => {
                if (err) {
                    console.log(err);
                }
                else {

                    console.log(`all the lists for the Thanos board :-`);
                    console.log(listData);
                    let cardId = listData.find((data) => data.name == 'Mind').id;


                    returnCards(cardId, cardObject, (err, cardData) => {

                        if (err) {
                            console.log(err);
                        }
                        else {
                            console.log(`all cards for the Mind list ;-`);
                            console.log(cardData);
                        }
                    })
                }
            })
            console.log(board);
        }
    })
}

module.exports = getAllDatafromMindList;